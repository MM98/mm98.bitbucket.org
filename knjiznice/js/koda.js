
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
 var ix = 0;
 var ehrIdarray = new Array(3);
 var control = false;
 
function generirajPodatke(stPacienta) {
  //var authorization = getAuthorization();
  var ehrId = "";
  
  if(stPacienta == 1) {
    var ime = "Zaphod"
    var priimek = "Beeblebrox"
    var datumRojstva = "1980-03-03" + "T00:00:00.000Z";
  }
  else if(stPacienta == 2) {
    var ime = "Arthur";
    var priimek = "Dent";
    var datumRojstva = "1979-08-10" + "T00:00:00.000Z";
  }
  else if(stPacienta == 3) {
    var ime = "Tricia";
    var priimek = "McMillian";
    var datumRojstva = "1983-04-11" + "T00:00:00.000Z";
  }
  
  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
      ehrId = data.ehrId;
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
          if(ix == 2) {
            control = true;
          }
          ehrIdarray[ix] = ehrId;
          console.log(ehrIdarray[ix]);
          ix++;
          //$("#preberiEHRid").val(ehrId);
          
          /*if(party.action == 'CREATE') {
            if(stPacienta==1) {
              dodajMeritve(ehrIdarray[0], "179.2", "98.3", "2017-05-20T09:30", "36.2", "111", "77", "93");
              dodajMeritve(ehrIdarray[0], "179.2", "99.0", "2018-03-11T11:22", "36.5", "121", "84", "88");
              dodajMeritve(ehrIdarray[0], "179.2", "102.4", "2019-02-09T23:11", "37.2", "119", "79", "92");
            }
            else if(stPacienta==2) {
              dodajMeritve(ehrIdarray[1], "174.5", "88.1", "2017-04-20T09:30", "36.4", "108", "79", "98");
              dodajMeritve(ehrIdarray[1], "174.5", "85.4", "2018-01-11T11:22", "36.9", "111", "82", "94");
              dodajMeritve(ehrIdarray[1], "174.5", "82.5", "2019-05-09T23:11", "35.9", "114", "75", "88");
            }
            else if(stPacienta==3) {
              dodajMeritve(ehrIdarray[2], "166.8", "56.1", "2017-06-20T09:30", "36.5", "115", "75", "93");
              dodajMeritve(ehrIdarray[2], "166.8", "55.4", "2018-03-11T11:22", "36.8", "101", "83", "96");
              dodajMeritve(ehrIdarray[2], "166.8", "52.5", "2019-01-09T23:11", "35.4", "124", "71", "91");
            }
          }*/
          
        }
      });
    }
  });
  return ehrId;
}

function preberiMeritve() {
  var erid = document.getElementById("vitalniEHRid").value;
  
  $.ajax({
    url: baseUrl + "/view/" + erid + "/height",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      var ix = 0;
      for(var i in res) {
        ix++;
      }
      document.getElementById("znaniEHRHeight").value = res[ix-1].height;
    },
    error: function(err) {
      console.log("Napaka " + err.responseText);
    }
  });
  
  $.ajax({
    url: baseUrl + "/view/" + erid + "/weight",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      var ix = 0;
      for(var i in res) {
        ix++;
      }
      document.getElementById("znaniEHRWeight").value = res[ix-1].weight;
    },
    error: function(err) {
      console.log("Napaka " + err.responseText);
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + erid + "/body_temperature",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      var ix = 0;
      for(var i in res) {
        ix++;
      }
      document.getElementById("znaniEHRTemp").value = res[ix-1].temperature;
    },
    error: function(err) {
      console.log("Napaka " + err.responseText);
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + erid + "/body_temperature",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      var ix = 0;
      for(var i in res) {
        ix++;
      }
      document.getElementById("znaniEHRTemp").value = res[ix-1].temperature;
    },
    error: function(err) {
      console.log("Napaka " + err.responseText);
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + erid + "/blood_pressure",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      var ix = 0;
      for(var i in res) {
        ix++;
      }
      document.getElementById("znaniEHRSistolic").value = res[ix-1].systolic;
      document.getElementById("znaniEHRDiastolic").value = res[ix-1].diastolic;
    },
    error: function(err) {
      console.log("Napaka " + err.responseText);
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + erid + "/spO2",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      var ix = 0;
      for(var i in res) {
        ix++;
      }
      document.getElementById("znaniEHROxy").value = res[ix-1].spO2;
    },
    error: function(err) {
      console.log("Napaka " + err.responseText);
    }
  });
}

function dodajMeritve(ehrId, visina, teza, datumInUra, temp, sist, diast, oxy) {
 /* $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });*/
  
  var podatki = {
    "ctx/language": "en",
    "ctx/territory": "SI",
		"ctx/time": datumInUra,
		"vital_signs/height_length/any_event/body_height_length": visina,
		"vital_signs/body_weight/any_event/body_weight": teza,
	  "vital_signs/body_temperature/any_event/temperature|magnitude": temp,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		"vital_signs/blood_pressure/any_event/systolic": sist,
		"vital_signs/blood_pressure/any_event/diastolic": diast,
		"vital_signs/indirect_oximetry:0/spo2|numerator": oxy
  }
  var parametriZahteve = {
    ehrId: ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT'
    //committer: "Sestra Bablura"
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(podatki),
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      console.log($.param(parametriZahteve));
      console.log("DELA");
      console.log(res.meta.href);
    },
    error: function(err) {
      console.log("Napaka " + err.responseText);
    }
  });
}

//console.log(ix);

function generirajPodatkeKlik() {
  
  if(!control) {
    for(var i = 0; i < 3; i++) {
      generirajPodatke(i+1);
    }
    
    document.getElementById("modalHeader").innerHTML = "<h2>Podatki uspešno generirani.</h2>";
    
    setTimeout(function (){
      document.getElementById("0").innerHTML = "<p><strong>Zaphod Beeblebrox: " + ehrIdarray[0] + "</strong></p>";
      document.getElementById("1").innerHTML = "<p><strong>Arthur Dent: " + ehrIdarray[1] + "</strong></p>";
      document.getElementById("2").innerHTML = "<p><strong>Tricia McMillian: " + ehrIdarray[2] + "</strong></p>";
      dodajMeritve(ehrIdarray[0], "179.2", "98.3", "2017-12-10", "36.2", "111", "77", "93");
      dodajMeritve(ehrIdarray[1], "174.5", "88.1", "2017-12-10", "36.4", "108", "79", "98");
      dodajMeritve(ehrIdarray[2], "166.8", "56.1", "2017-12-10", "36.5", "115", "75", "93");
      openModal();
    }, 1000);
  }
  else {
    alert("Podatki so že generirani!");
  }
}

//function dodajVitalneZaZnane() {//TODO}

function vitalniZnakiZnanih() {
  var x = document.getElementById("vitalniZnakiID").value;
  
  if(x == -1) {
    document.getElementById("vitalniEHRid").value = "";
  }
  else {
    if(!control) {
      alert("Generirajte podatke s klikom na gumb.");
    } else {
      document.getElementById("vitalniEHRid").value = ehrIdarray[x];
    }
  }
}


function ehrIdPreverjanje() {
  var y = document.getElementById("preberiObstojeciEHR").value;
  console.log(y);
   if(y == -1) {
    document.getElementById("preberiEHRid").value = "";
  }
  else {
    if(!control) {
      alert("Generirajte podatke s klikom na gumb.");
    } else {
      document.getElementById("preberiEHRid").value = ehrIdarray[y];
    }
  } 
}

function preveriEHR() {
  var eid = document.getElementById("preberiEHRid").value;
  console.log(eid);
  
  $.ajax({
    url: baseUrl + "/demographics/ehr/" + eid + "/party",
    type: 'GET',
    headers:{
      "Authorization": getAuthorization()
    },
    success: function (data) {
      var oseba = data.party;
      document.getElementById("preberiSporocilo").innerHTML = "<span class='obvestilo label " +
      "label-success fade-in'>Bolnik/-ca '" + oseba.firstNames + " " +
      oseba.lastNames + "', ki se je rodil/-a '" + oseba.dateOfBirth +
      "'.</span>";
    },
    error: function(err) {
      console.log("Napaka " + err.responseText);
    }
  });
}

var brewerydburl = "https://sandbox-api.brewerydb.com/v2/";
var key = "3f68c5e17744ca7d648cc620ef4e61fb";

function getRandomBeer() {
  $.getJSON("https://cors-anywhere.herokuapp.com/https://sandbox-api.brewerydb.com/v2/beer/random/?key=3f68c5e17744ca7d648cc620ef4e61fb", function(data) {
    var pivo = JSON.stringify(data);
    //var test = JSON.parse(data);
    console.log(pivo);
    //console.log(pivo.data.name);
    //console.log(pivo.name);
    //console.log(pivo.name);
    var test = data.data;
    console.log("Ime: " + test.name);
    console.log("Alko: " + test.abv);
    console.log("Stil: " + test.style.shortName);
    console.log("Opis: " + test.style.description);
    
    document.getElementById("imePiva").value = test.name;
    document.getElementById("alkoPiva").value = test.abv;
    document.getElementById("stilPiva").value = test.style.shortName;
    document.getElementById("opisPiva").innerHTML = test.style.description;
  });
}

//var span = document.getElementsByClassName("close")[0];

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

window.onclick = function(event) {
  if (event.target ==  document.getElementById("myModal")) {
     document.getElementById("myModal").style.display = "none";
  }
}

function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function preostanekLet() {
  var leta = Math.floor(Math.random() * 35) + 1;
  alert("Ostalo vam je " + leta + " let! Na zdravje!");
  getRandomBeer();
}

window.onload = function () {
  const CHART = document.getElementById("myCanvas");
  var myCanvas = new Chart(CHART, {
    type: "line",
    data: {
          labels: ['Prvi obisk', 'Drugi obisk', 'Tretji obisk', 'Četrti obisk', 'Peti obisk', 'Šesti obisk'],
          datasets: [{
              label: 'Nivo zadovoljstva uporabnikov te strani glede na število obiskov',
              data: [60, 95, 15, 25, 10, 5],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          }]
      }
  });
}